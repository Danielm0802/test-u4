# Instrucciones

- Realice un fork de la app:
  ![Fork](https://docs.gitlab.com/ee/user/project/repository/img/forking_workflow_fork_button_v13_10.png)
- Agregue los siguientes usuarios como "developer":

        - a.sandoval
        - s2c52
        - rakonz
        - l.pichintines01
        - mnegrier

- Clone el repositorio en su equipo

        git clone <repositorio>

- Inicialice el flujo de trabajo

        git flow init

- Agregue sus cambios al repositorio local

        git add . && git commit -m "Comentario"

- Agregue sus cambios al repositorio remoto.

        git push origin develop
